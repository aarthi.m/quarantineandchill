const express = require('express')
const user = express()
const bodyParser = require('body-parser')
const async = require('async')
const dotenv = require('dotenv')
dotenv.config()

const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const mongoose = require('mongoose')
const validator = require('email-validator')

const connection = require('../../model/user.model.js')
const userModel = mongoose.model('users')
const requestModelConnection = require('../../model/request.model.js')
const requestModel = mongoose.model('requests')
const friendModelConnection = require('../../model/friend.model.js')
const friendModel = mongoose.model('friends')

user.use(bodyParser.urlencoded({
  extended: false
}))
user.use(bodyParser.json())

user.post('/friend', (req,res) => {
  friendModel.find({ user2 : req.body.userName },(err, doc) => {
    console.log("post /user/friend");
    console.log(req.body.userName);
    console.log(doc);
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: false,
          msg: 'No Data'
        })
      } 
      else {
        const friends = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
          
            friendName:doc[i].user1,
          }
          friends.push(data)
        }
        const d = {
          Friends: friends
        }
        //console.log(d);
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: "Request Details couldn't be fetched. Please try again"
      })
    }
  })
})

user.post('/reject', (req,res) => {
  //body sender and receiver name
     console.log(req.body);
    
         requestModel.remove({ senderName : req.body.senderName}, (err,doc) => {
           if(!err){
             return res.status(200).send({
               auth: true,
               data: doc,
               msg:  ' removed from requests'
             })
           }
           else{
             return res.status(400).send({
               msg: 'not removed from requests'
             })
           }
         })
})

user.post('/accept', (req,res) => {
   //body sender and receiver name
      console.log(req.body);
      const friend = new friendModel();
      friend.user1 = req.body.senderName;
      friend.user2 = req.body.receiverName;
      friend.save((err, doc) => {
        if (!err) {
          //console.log(doc);
          requestModel.remove({ senderName : req.body.senderName}, (err,doc) => {
            if(!err){
              return res.status(200).send({
                auth: true,
                data: doc,
                msg:  ' Friends registered succesfully and removed from requests'
              })
            }
            else{
              return res.status(400).send({
                msg: 'Friends registered succesfully and not removed from requests'
              })
            }
          })
         
        } else {
            //console.log(err);
          return res.status(404).send({
            msg: 'Friends not registered, try again'
          })
        }
      })
})

user.get('/request', (req,res) => {
  requestModel.find((err, doc) => {
    console.log("get /user/request");
    //console.log(doc);
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'No Data'
        })
      } 
      else {
        const requests = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            requestId: doc[i]._id,
            senderName: doc[i].senderName,
            receiverName:doc[i].receiverName,
          }
          requests.push(data)
        }
        const d = {
          Requests: requests
        }
        //console.log(d);
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: "Request Details couldn't be fetched. Please try again"
      })
    }
  })
})
user.post('/friendrequest', (req,res) => {

  // async.parallel([
	// 	function(callback) {
			//if(req.body.receiverName) {
          
        const reqReg = new requestModel();
        reqReg.senderName = req.body.senderName;
        reqReg.receiverName = req.body.receiverName;
        reqReg.save((err, doc) => {
          if (!err) {
            //console.log(doc);
            return res.status(200).send({
              auth: true,
              data: doc,
              msg:  ' request registered succesfully'
            })
          } else {
              //console.log(err);
            return res.status(404).send({
              msg: 'request not registered, try again'
            })
          }
        })
      })
                               

user.get('/dashboard',( req,res) => {
  userModel.find((err, doc) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'No Data'
        })
      } else {
        const users = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            userId: doc[i]._id,
            userName: doc[i].userName,
            userStatus:doc[i].userStatus,
          }
          users.push(data)
        }
        const d = {
          Users: users
        }
        // console.log(String(Date()));
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: "Users Details couldn't be fetched. Please try again"
      })
    }
  })
})

user.post('/profile',(req,res) => {
      userModel.find({userEmail : req.body.userEmail} ,(err,doc) => {
        console.log("authcontroller line 24 , user/profile post method");
        console.log(doc);
        console.log(req.body);
        if(err){
          return res.status(500).send({
            msg : 'user profile is not fetched'
           })
        }
        else{
          userModel.findOneAndUpdate( { userEmail : req.body.userEmail }, 
                                     { $set: { userStatus: req.body.userStatus}}, 
                                     (err,doc) => {
            if(err){
              return res.status(500).send({
                msg : 'user status not updated'
               })
            }
            else{
              console.log("line 39");
              console.log(doc);
              return res.status(200).send({
                auth : true,
                //msg : 'user status updated'
               })
            }
          })
        }
      })
})

user.post('/', (req, res) => {
    console.log(req.body);
    const email = req.body.userEmail;
    userModel.find({ userEmail: email} , (err,doc) => {
      if(err){
          return res.status(500).send({
              msg : 'user profile is not fetched'
          })
      }
      else{
       console.log("AuthController/line 61");
       console.log(doc);

          return res.status(200).send({
              auth: true,
              data: doc,
              msg: 'user details fetched'
          })
          //console.log(data);
      }
  })
})

user.post('/register', async (req, res) => {
  if (!req.body.userName || !req.body.userEmail || !req.body.userPassword || !req.body.userPhone) {
    return res.status(400).send({
      msg: 'Bad payload'
    })
  }

  if (!validator.validate(req.body.userEmail)) {
    return res.status(404).send({
      msg: 'Email badly formatted'
    })
  }
  // console.log(req.body.password);
  const pwd = await bcrypt.hashSync(req.body.userPassword, 8)
  const userReg = new userModel()
  userReg.userName = req.body.userName
  userReg.userEmail = req.body.userEmail
  userReg.userPassword = pwd
  userReg.userPhone = req.body.userPhone
  userReg.userStatus=req.body.userStatus
  userReg.createdAt = new Date()
  await userReg.save((err, doc) => {
    if (!err) {
      console.log(doc);
      return res.status(200).send({
        data: doc,
        msg: req.body.userName + ' your details registered succesfully'
      })
    } else {
        console.log(err);
      return res.status(404).send({
        msg: 'User Details not registered, try again'
      })
    }
  })
})

user.post('/login', async (req, res) => {
  if (!req.body.userEmail || !req.body.userPassword) {
    return res.status(400).send({
      auth: false,
      msg: 'Bad payload'
    })
  }
  // console.log(req.body.email,req.body.password);
  await userModel.find({ userEmail: req.body.userEmail }, (err, doc) => {
    console.log("authController line 115");
    console.log(doc);
    console.log(req.body.userPassword);
    if (!err) {
      if (doc.length == 0) {
        return res.status(404).send({
          auth: false,
          msg: 'Invalid Email'
        })
      } else {
        const encryptedPassword = doc[0].userPassword
        // console.log(doc[0].userPassword, req.body.password);
        //const passwordIsValid = bcrypt.compareSync(req.body.userPassword, encryptedPassword)
        //const passwordIsValid = 
        // if ( req.body.userPassword == encryptedPassword ) {
        //   return res.status(404).send({
        //     auth: false,
        //     msg: 'Invalid Password'
        //   })
        // } else {
          const token = jwt.sign({
            id: doc[0]._id,
            email: doc[0].userEmail
          }, process.env.jwtSecret, {
            expiresIn: 86400000 * 15 // 15 days
          })

          const data = {
            id: doc[0]._id,
            userName: doc[0].userName,
            userEmail: doc[0].userEmail,
            userPhone: doc[0].userPhone,
            userPassword: doc[0].userPassword
          }

          return res.status(200).send({
            auth: true,
            token: token,
            msg: 'Login success :)',
            data: data
          })
        //}
      }
    } else {
      return res.status(500).send({
        auth: false,
        msg: 'DB error'
      })
    }
  })
})

module.exports = user