const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: 'Required'
  },
  userEmail: {
    type: String,
    required: 'Required',
    unique: true
  },
  userPassword: {
    type: String,
    required: 'Required'
  },
  userPhone: {
    type: String,
    required: 'Required',
    unique: true
  },
  userImage : {
    type:String,
  },
  userStatus:{
      type: String
  },
  sentRequest:[{
    receiverName: {type: String, default: ''}
  }],
  receivedRequest:[{
        senderId: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
		senderName: {type: String, default: ''}
  }],
  friendList:[{
        friendId: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
        friendName: {type: String, default: ''}
  }],
  totalRequest: {
      type: String, 
      default:0
  },
  createdAt: {
    type: Date,
    required: 'Required',
    default: Date.now
  }
})

mongoose.model('users', userSchema);

